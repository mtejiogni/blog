import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
	posts: {
	  title: string,
	  content: string,
	  loveIts: number,
	  created_at: Date
	}[] = [

		{
		  title : 'Le TypeScript',
		  content : 'TypeScript est un langage très récent (février 2012) qui a été conçu par Anders Hejlsberg, egalement concepteur du langage C#. Le but premier de TypeScript est de rendre plus facile et plus fiable l\'écriture de code en JavaScript pour des applications de grande ampleur.',
		  loveIts : 5,
		  created_at : new Date()
		},

		{
		  title : 'La Transcompilation',
		  content : 'transcompilation : "traduction" d\'un langage de programmation vers un autre - différent de la compilation, qui transforme généralement le code vers un format exécutable',
		  loveIts : 6,
		  created_at : new Date()
		},

		{
		  title : 'Angular',
		  content : 'Il y a plusieurs frameworks JavaScript très populaires aujourd’hui : Angular, React, Ember, Vue… les autres frameworks marchent très bien, ont beaucoup de succès et sont utilisés sur des sites extrêmement bien fréquentés, React et Vue notamment. Angular présente également un niveau de difficulté légèrement supérieur, car on utilise le TypeScript plutôt que JavaScript pur ou le mélange JS/HTML de React.',
		  loveIts : -3,
		  created_at : new Date()
		}

	];

}
