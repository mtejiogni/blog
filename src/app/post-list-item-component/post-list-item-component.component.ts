import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list-item-component',
  templateUrl: './post-list-item-component.component.html',
  styleUrls: ['./post-list-item-component.component.scss']
})
export class PostListItemComponentComponent implements OnInit {

	@Input() post: {
	  title: string,
	  content: string,
	  loveIts: number,
	  created_at: Date
	};


  	constructor() {
  	}


  	ngOnInit() {
  	}



  	/*
  		Gestion des Loveits
  	*/
  
  	loveit(love: boolean) {
  		if(love) {
  			this.post.loveIts ++;
  		}
  		else {
  			this.post.loveIts --;
  		}
  	}




  	/*
  		Quand un post a plus de "love it" que de "don't love it" (loveIts > 0), il sera coloré en vert, et inversement quand loveIts < 0, il sera coloré en rouge. 
  	*/
  	getClassgroup() {
  		if(this.post.loveIts >= 0) {
  			return [true, false];
  		}
  		else {
  			return [false, true];
  		}
  	}

}
